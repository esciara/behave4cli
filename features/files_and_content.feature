Feature: Create files with content

    @setup
    Scenario: Setup a working directory
        Given a new working directory

    Scenario: Create empty files
        Given an empty file named "empty_file"
        Then the file named "empty_file" should exist

    Scenario: Create files with content
        Given a file named "non_empty_file" with:
        """
        Hello World!
        """
        Then the file named "non_empty_file" should exist
        And the file "non_empty_file" should contain "Hello"
        And the file "non_empty_file" should contain
        """
        World
        """

    Scenario: Create files at a directory
        Given an empty file named "my/directory/empty_file"
        Then the file named "my/directory/empty_file" should exist
        And the directory "my/directory" exists

